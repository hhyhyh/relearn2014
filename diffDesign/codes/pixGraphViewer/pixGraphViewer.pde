int screenH = 600;
int screenW = 1200;
int screenGUI = 0;

PImage pixData;

faceObj face;
ArrayList<faceObj> slicedFaces;

int stlScale = 10;

void setup() {

 noLoop();
 size(screenW, screenH,OPENGL);

 //////////////////establish twitter connection
 connectTwitter();
 //////////////////establish twitter connection
 
 slicedFaces = new ArrayList<faceObj>();
 initializeFaces();

}

void draw() {
  
  background(0);
  
  if(tweetImage==true){
    println("tweeting...");
    tweetNewImage();
    tweetImage=false;
  }

  pushMatrix();
  
    directionalLight(125, 125, 125, 0, 1, -1);
    directionalLight(125, 125, 125, 0, 1, 1);
    directionalLight(125, 125, 125, 0, -1, -1);
    
    ambientLight(75,75,75);
    translate((width/6)*3,height/2,0);
    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    for (int i=0; i<slicedFaces.size(); i++){ 
      faceObj tempFace = slicedFaces.get(i);
      if (displaySlices==true){
        tempFace.displaySlices(); 
      } else{
        tempFace.displayMesh(); 
      }
    }
    noFill();
    stroke(255);
    strokeWeight(1/SCALE);
    box(16777216*2,16777216*2,16777216*2);
    
   popMatrix();
 
   noLights();
   image(pixData, 0, 0);
  
  if(makeTweetImage==true){
    makeTweetImage();
    makeTweetImage=false;
    tweetImage=true;
  }

}
