class faceObj{
  
  float [] vA = new float [3];
  float [] vB = new float [3];
  float [] vC = new float [3];
  
  float [] vN = new float [3];
  
  int rVal;
  float rValMap;
  
  faceObj(int a0, int a1, int a2, int b0, int b1, int b2, int c0, int c1, int c2, int row ){
    
    vA[0] = float(a0)+ (colRange/2);
    vA[1] = float(a1)+ (colRange/2);
    vA[2] = float(a2)+ (colRange/2);
    
    vB[0] = float(b0)+ (colRange/2);
    vB[1] = float(b1)+ (colRange/2);
    vB[2] = float(b2)+ (colRange/2);
    
    vC[0] = float(c0)+ (colRange/2);
    vC[1] = float(c1)+ (colRange/2);
    vC[2] = float(c2)+ (colRange/2);
    
    rVal = 254-row;
    rValMap = map(rVal, 0, 255, colRangeMin, colRangeMax) + (colRange/2);
    
  }
  
  void displayMesh(){
    
    strokeWeight(1/SCALE);
    stroke(0,20); 
    fill(255);
    
    beginShape();
      vertex(vA[0], vA[1], vA[2]);
      vertex(vB[0], vB[1], vB[2]);
      vertex(vC[0], vC[1], vC[2]);
    endShape(CLOSE);  
    
  }
  
  void displaySlices(){
    
    strokeWeight(100);
    stroke(255); 
    fill(255);
    
    for (int i=0; i<65793; i+=10000){
      beginShape();
        vertex(vA[0], vA[1], rValMap);
        vertex(vB[0], vB[1], rValMap);
        vertex(vC[0], vC[1], rValMap);
      endShape(CLOSE); 
    }  
    
  }
}
/////////////////////////////////////////////////////////////



