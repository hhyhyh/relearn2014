float ROTX = 0;                        
float ROTY = 0;
float SCALE = 1;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////control the rotation and scale display of the model using the mouse
void mouseDragged() {
   
   if ((mouseY>screenGUI/2)&&(mouseY<screenH+(screenGUI/2))){
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;
     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
          SCALE = SCALE+0.1;
       } else {
          SCALE = SCALE-0.1;
       }
       if (SCALE<0.2){ SCALE=0.2;}
       if (SCALE>=10){ SCALE=10;}
     }
     redraw();
   }
   
}

////press spacebar to send a tweet of the pixGrpah
void keyReleased() {
  if ( keyCode == 32) {
    makeTweetImage=true;
  }
}



