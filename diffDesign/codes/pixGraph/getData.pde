String stlFile = "data/man_5.stl";

float minX = 255000;
float maxX = -255000;
float minY = 255000;
float maxY = -255000;
float minZ = 255000;
float maxZ = -255000;

float xRange;
float yRange;
float zRange;

////////////////////////////////////////////////////////////////////////////////////////
////load an stl model file from the loaction in the sketch specificed above
////convert the faces into a table of vertiices
////calcualte the bounding box of the model data

void loadStlData(){
  mesh = (TriangleMesh)new STLReader().loadBinary(sketchPath(stlFile),STLReader.TRIANGLEMESH);
  gfx = new ToxiclibsSupport(this);
  
  facesList = new ArrayList<Face>();
  facesList = mesh.faces;
  int temp = facesList.size();
  println(temp);
  
  vTable = new Table();
  vTable.addColumn("x0");
  vTable.addColumn("y0");
  vTable.addColumn("z0");
  vTable.addColumn("x1");
  vTable.addColumn("y1");
  vTable.addColumn("z1");
  vTable.addColumn("x2");
  vTable.addColumn("y2");
  vTable.addColumn("z2");
  
  //println("faces <> " + facesList.size());
  
  for (int i = 0; i<facesList.size(); i++) {
    TableRow newRow =vTable.addRow();
    
    Face face = facesList.get(i);
    newRow.setFloat("x0",face.a.x);
    newRow.setFloat("y0",face.a.y);
    newRow.setFloat("z0",face.a.z);
    
    newRow.setFloat("x1",face.b.x);
    newRow.setFloat("y1",face.b.y);
    newRow.setFloat("z1",face.b.z);

    newRow.setFloat("x2",face.c.x);
    newRow.setFloat("y2",face.c.y);
    newRow.setFloat("z2",face.c.z);
    
    ///CALCULATE X,Y,Z RANGE BASED ON EACH VERTEX
    float tempX;
    tempX = (face.a.x);
    if (tempX<minX){ minX=tempX;}
    if (tempX>maxX){ maxX=tempX;}
    tempX = (face.b.x);
    if (tempX<minX){ minX=tempX;}
    if (tempX>maxX){ maxX=tempX;}
    tempX = (face.c.x);
    if (tempX<minX){ minX=tempX;}
    if (tempX>maxX){ maxX=tempX;}
    
    float tempY;
    tempY = (face.a.y);
    if (tempY<minY){ minY=tempY;}
    if (tempY>maxY){ maxY=tempY;}
    tempY = (face.b.y);
    if (tempY<minY){ minY=tempY;}
    if (tempY>maxY){ maxY=tempY;}
    tempY = (face.c.y);
    if (tempY<minY){ minY=tempY;}
    if (tempY>maxY){ maxY=tempY;}
    
    float tempZ;
    tempZ = (face.a.z);
    if (tempZ<minZ){ minZ=tempZ;}
    if (tempZ>maxZ){ maxZ=tempZ;}
    tempZ = (face.b.z);
    if (tempZ<minZ){ minZ=tempZ;}
    if (tempZ>maxZ){ maxZ=tempZ;}
    tempZ = (face.b.z);
    if (tempZ<minZ){ minZ=tempZ;}
    if (tempZ>maxZ){ maxZ=tempZ;}
  }

  saveTable(vTable, "data/mesh.csv");
  
  xRange = maxX-minX;
  yRange = maxY-minY;
  zRange = maxZ-minZ;
  
  println(xRange + " <> " +yRange + " <> " + zRange);
  
  println("xRange: " + minX + " <> " + maxX);
  println("yRange: " + minY + " <> " + maxY);
  println("zRange: " + minZ + " <> " + maxZ);
  
  println("loaded");
  
}

