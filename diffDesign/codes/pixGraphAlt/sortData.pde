PGraphics pg;
PGraphics pgTrans;

int pixColCount;

int maxWidth=0;
int maxWidthTemp=0;

int colCount;

int imageHeight = 255;

//int colRangeMin = -2147483648;
//int colRangeMax = 2147483647;

int colRangeMin = -16777216;
int colRangeMax = -1;
int colRange = colRangeMax-colRangeMin;

int maxFactor = 3;

float minScale;
float maxScale;

////////////////////////////////////////////////////////////////////////////////////////
///sort the faces data into a vertical pixel graph, accoding to the size of the processing sketch window
///group faces accoring to the average z value of each face, aranged vertically, and stack the data horizontally

////calculate the size of the image, width as well as height that will be needed
////create a .png file of the graph

void getGraphSize(){
  
  for (int j=0; j<=imageHeight; j++){ 
    
    pixColCount=0;
    maxWidthTemp=0;
    
    for (int i=0; i<vTable.getRowCount(); i++){ 
      ///get average z height of face anbd map to the screen height
      float tempZVal = (vTable.getFloat(i, "z0") + vTable.getFloat(i, "z1") + vTable.getFloat(i, "z2"))/3;
      int pPosZ = int(map(int(tempZVal), minZ, maxZ, 0, imageHeight));
      
      if (maxWidthTemp>maxWidth){
        maxWidth=maxWidthTemp;
      }
      if (pPosZ==j){
        maxWidthTemp+=9;
      }  
    }
  }  
  println("maxWidth" + maxWidth);
  
///////////////////////////////////////////determine the largest dimension of the bounding box.....
  if (xRange>yRange) {
    if (xRange>zRange) {
      maxFactor=0;
      minScale = minX;  
      maxScale = maxX; 
    } else {
      maxFactor=2;
      minScale = minZ;  
      maxScale = maxZ; 
    }    
  } else { 
    if (yRange>zRange) {
      maxFactor=1; 
      minScale = minY;  
      maxScale = maxY; 
    } else {
      maxFactor=2; 
      minScale = minZ;  
      maxScale = maxZ;  
    }    
  }
  
  println("maxFactor " + maxFactor);
  
}

////sort the data according to z heights vertically, stacking horizontally
void sortGraph(){

  pg = createGraphics(maxWidth, imageHeight);
  pg.beginDraw();
  pg.background(0,0);

  for (int j=0; j<imageHeight; j++){ 
    
    pixColCount=0;
 
    for (int i=0; i<vTable.getRowCount(); i++){ 
      
      ///get average z height of face and map to the screen height
      float tempZVal = (vTable.getFloat(i, "z0") + vTable.getFloat(i, "z1") + vTable.getFloat(i, "z2"))/3;
      int pPosZ = int(map(int(tempZVal), minZ, maxZ, 0, imageHeight));

      if (pPosZ==j){
        
        for (int k=0; k<3; k++){
          
          String xColumn = "x" + str(k);
          String yColumn = "y" + str(k);
          String zColumn = "z" + str(k);
          
////////////////////////////////////////////////////////////////////////////////X 
          float tempX = vTable.getFloat(i, xColumn);
//          int tempColX = int(tempX);  

          //tempX = tempX-xRange/2;
          
          int tempColX = int(map(int(tempX), minScale, maxScale, colRangeMin+(colRange/2), colRangeMax+(colRange/2)));
          //println(tempColX);
          
          color x = tempColX;
          pg.set(pixColCount,imageHeight-j,x);  
          pg.updatePixels();     
//////////////////////////////////////////////////////////////////////////////////Y       
          float tempY = vTable.getFloat(i, yColumn);
//          int tempColY = int(tempY);
          //tempY = tempY-yRange/2;
          
          int tempColY = int(map(int(tempY), minScale, maxScale, colRangeMin+(colRange/2), colRangeMax+(colRange/2)));
          //println(tempColY);
          
          color y = tempColY;
          pg.set(pixColCount+1,imageHeight-j,y);
          pg.updatePixels();
//////////////////////////////////////////////////////////////////////////////////Z       
          float tempZ = vTable.getFloat(i, zColumn);
//          int tempColZ = int(tempZ);
          //tempZ = tempZ-zRange/2;
          
          int tempColZ = int(map(int(tempZ), minScale, maxScale, colRangeMin+(colRange/2), colRangeMax+(colRange/2)));

          //println(tempColY);
          
          color z = tempColZ;
          pg.set(pixColCount+2,imageHeight-j,z);
          pg.updatePixels();
          
          pixColCount+=3;          
         
        }
        
      }
    }
    
  }

  pg.updatePixels();
  pg.endDraw();
  pg.save("data/pixGraph.png");
  
}





