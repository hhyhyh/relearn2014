String stlFile = "data/Swirl2.stl";

float minX = 16777216;
float maxX = -16777216;
float minY = 16777216;
float maxY = -16777216;
float minZ = 16777216;
float maxZ = -16777216;

float xRange;
float yRange;
float zRange;

float xRangeMap;
float yRangeMap;
float zRangeMap;

int stlScale = 10;

////////////////////////////////////////////////////////////////////////////////////////
////load an stl model file from the loaction in the sketch specificed above
////convert the faces into a table of vertiices
////calcualte the bounding box of the model data

void loadStlData(){
  mesh = (TriangleMesh)new STLReader().loadBinary(sketchPath(stlFile),STLReader.TRIANGLEMESH);
  gfx = new ToxiclibsSupport(this);
  
  facesList = new ArrayList<Face>();
  facesList = mesh.faces;
  int temp = facesList.size();
  println(temp);
  
  vTable = new Table();
  vTable.addColumn("x0");
  vTable.addColumn("y0");
  vTable.addColumn("z0");
  vTable.addColumn("x1");
  vTable.addColumn("y1");
  vTable.addColumn("z1");
  vTable.addColumn("x2");
  vTable.addColumn("y2");
  vTable.addColumn("z2");
  
  //println("faces <> " + facesList.size());
  
  for (int i = 0; i<facesList.size(); i++) {
    TableRow newRow =vTable.addRow();
    
    Face face = facesList.get(i);
    newRow.setFloat("x0",face.a.x*stlScale);
    newRow.setFloat("y0",face.a.y*stlScale);
    newRow.setFloat("z0",face.a.z*stlScale);
    
    newRow.setFloat("x1",face.b.x*stlScale);
    newRow.setFloat("y1",face.b.y*stlScale);
    newRow.setFloat("z1",face.b.z*stlScale);

    newRow.setFloat("x2",face.c.x*stlScale);
    newRow.setFloat("y2",face.c.y*stlScale);
    newRow.setFloat("z2",face.c.z*stlScale);
    
    ///CALCULATE X,Y,Z RANGE BASED ON EACH VERTEX
    float tempX;
    tempX = (face.a.x*stlScale);
    if (tempX<minX){ minX=tempX;}
    if (tempX>maxX){ maxX=tempX;}
    tempX = (face.b.x*stlScale);
    if (tempX<minX){ minX=tempX;}
    if (tempX>maxX){ maxX=tempX;}
    tempX = (face.c.x*stlScale);
    if (tempX<minX){ minX=tempX;}
    if (tempX>maxX){ maxX=tempX;}
    
    float tempY;
    tempY = (face.a.y*stlScale);
    if (tempY<minY){ minY=tempY;}
    if (tempY>maxY){ maxY=tempY;}
    tempY = (face.b.y*stlScale);
    if (tempY<minY){ minY=tempY;}
    if (tempY>maxY){ maxY=tempY;}
    tempY = (face.c.y*stlScale);
    if (tempY<minY){ minY=tempY;}
    if (tempY>maxY){ maxY=tempY;}
    
    float tempZ;
    tempZ = (face.a.z*stlScale);
    if (tempZ<minZ){ minZ=tempZ;}
    if (tempZ>maxZ){ maxZ=tempZ;}
    tempZ = (face.b.z*stlScale);
    if (tempZ<minZ){ minZ=tempZ;}
    if (tempZ>maxZ){ maxZ=tempZ;}
    tempZ = (face.b.z*stlScale);
    if (tempZ<minZ){ minZ=tempZ;}
    if (tempZ>maxZ){ maxZ=tempZ;}
  }

  saveTable(vTable, "data/mesh.csv");
  
  xRange = maxX-minX;
  yRange = maxY-minY;
  zRange = maxZ-minZ;
  
  xRangeMap = map(xRange, minX, maxX, colRangeMin, colRangeMax);
  yRangeMap = map(yRange, minY, maxY, colRangeMin, colRangeMax);
  zRangeMap = map(zRange, minZ, maxZ, colRangeMin, colRangeMax);
  
  println(xRange + " <> " +yRange + " <> " + zRange);
  println(xRangeMap + " <> " +yRangeMap + " <> " + zRangeMap);
  
  println("xRange: " + minX + " <> " + maxX);
  println("yRange: " + minY + " <> " + maxY);
  println("zRange: " + minZ + " <> " + maxZ);
  
  println("loaded");
  
}

