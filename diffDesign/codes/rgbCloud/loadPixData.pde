ArrayList<String> pixList;
String path;
Boolean getNewPixels = false;
PImage dotImage;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////searches the folder for all files. returns an arraylist of the files oin the folder
void getFileList(){
  
  path = savePath("");
  pixList = new ArrayList<String>();
  
  File folder = new File(path + "/data");
  File[] listOfFiles = folder.listFiles();

  for (File file : listOfFiles) {
    if (file.isFile()) {
      pixList.add(file.getName());
    }
  }
}

////load the active image file
void getPixels(){
  
  strPix.clear();
  
  clear(); 
  dotImage = loadImage(buttonSlide.activeFile);
  image(dotImage, 0, 0);
  loadPixels();
  updatePixels();
  
  ////looop throught the pixels in the image - a 2D array - and cretae a list of 'active values' in the RGB cube where there is a colour match
  for (int x=0; x<dotImage.width; x++){
    for (int y=0; y<dotImage.height; y++){
      
      color testCol = get(x, y);
      int hX = int(red(testCol));
      int hY = int(green(testCol));
      int hZ = int(blue(testCol)); 

      String tempPix = str(hX) + "," + str(hY) + "," + str(hZ);
      //println(tempPix);
      strPix.add(tempPix);
         
    }
  }
  ////remove the duplicate values from the list, to reduce the number of points drawn
  HashSet hs = new HashSet(strPix);
  println(strPix.size());
  strPix.clear();
  strPix.addAll(hs); 
  println(strPix.size());
 
}





