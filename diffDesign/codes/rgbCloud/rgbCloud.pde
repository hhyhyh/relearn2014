import java.util.*;

int screenH = 600;
int screenGUI = 80;
int guiDim= 20;
int screenW = 1200;
int totalPix=0;

pointObject pObj;
buttonSlider buttonSlide;
buttonText buttonTextBox;
enviro enviroBox;

ArrayList<pointObject> pObjs;
ArrayList<String> strPix;

void setup(){
  
  noLoop();
  size(screenW, screenH+screenGUI, OPENGL);
  background(255);
  
  //////////////////establish twitter connection
  connectTwitter();
  //////////////////establish twitter connection
  
  pObjs = new ArrayList<pointObject>(); 
  strPix = new ArrayList<String>(); 
  
  getFileList();
  initializeSliders();
  initializeTextBox();
  initializeEnviro();
  getPixels();
}

void draw(){
  
 if (searchTwitter==true){
    twitterSearches();
    getFileList();
    buttonSlide.update();
    searchTwitter=false;
  }
  
  background(0);

  if (showSearch==true){   
    buttonTextBox.display();
    showSearch=false;
  }
  
  if (getNewPixels==true){
    getPixels();
    println("redraw");
    getNewPixels = false;
  }

  buttonSlide.display();
  
  pushMatrix();
    translate(width/2, height/2);
    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    drawPoints();
    enviroBox.display();
  popMatrix(); 
  
  if(makeTweetImage==true){
    makeTweetImage();
    tweetNewImage();
    makeTweetImage=false;
  }
  
}














