Ten ways to network (using ssh)
===================================

* Connect to a network
* Use ifconfig to see your IP address(es), use ping to see others
* Associate an IP address with a name (/etc/hosts)
* ssh to a server
* Generate an ssh key and add them to (.ssh/authorized_keys) on the server
* Use scp to copy a file
* Mount remote file systems with sshfs
* Use apache to be create a drop box (/var/www, /usr/lib/cgi-bin, python)
* Use SSH as a git remote
* ...


Connect to a network
--------------------

DHCP is a protocol that assigns you an IP address, connects you to a gateway, and provides you with name servers (DNS).

Hosts, ports, IP (v4), and domain names.


Associate an IP address with a name (/etc/hosts)
-------------------------------------------------
The file /etc/hosts allows you to rename domain names to point to any IP adress.

Typically:

    127.0.0.1   localhost

localhost is a short cut for the special IP address 127.0.0.1 which always means "this machine".

You can connect the name "relearn.here" to the IP address 192.168.1.222, by adding the line:

    192.168.1.222   relearn.here

You can use the hash at the start of a line to add comments and temporarily swithc off names.

    # still waiting for the domain registration to be final
    # 79.99.202.57  deschaarbeeksetaal.be


Use ifconfig to see your IP address(es), use ping to see others
----------------------------------------------------------------

Actually your machine has an IP address for every *network interface* it has. Most laptops have two interfaces: a wireless one and a wired one (ethernet port). Each interface has an IP address to give it an identity on the network it connects to. With (IPv4) networks, this identiy is often not public, but meaningful only within a local


ssh to a server
----------------

    ssh user@server.name

You could use an IP address.

    ssh relearn@192.168.1.222



Generate an ssh key and add them to (.ssh/authorized_keys) on the server
-------------------------------------------------------------------------

Generating an ssh key (pair) allows you to login to a server without typing your password each time. In a nutshell, you generate a key, which makes a private part (id_rsa) which you *don't* share (it's secret), and a public part (id_rsa.pub) which you do. You then upload the public key to the server and add it to the .ssh/authorized_keys files. From that point on, when you ssh to the server, your local private file is compared with the remote public file and if they match, you are logged in without typing an additional password.

    ssh-keygen
    scp .ssh/id_rsa.pub relearn@relearn:yourname.pub
    ssh relearn@relearn
    cat yourname.pub >> .ssh/authorized_keys
    rm yourname.pub
    exit
    ssh relearn@relearn


Use scp to copy a file
-------------------------

    scp myfile user@server.name:path/to/whatever

For instance to copy a folder (recursive) to your public images folder

    scp -r 2014-06-20 user@server.name:httpdocs/images

Or the remote path may be absolute (slash after the colon)

    scp somebigfile.zip user@server.name:/var/www/vhosts/foo.bar/httpdocs/share


Mount remote file systems with sshfs
-------------------------------------

    mkdir mnt
    sshfs relearn@192.168.1.222:remote/path mnt



Use apache to be create a drop box (/var/www, /usr/lib/cgi-bin, python)
-------------------------------------------------------------------------


Use SSH as a git remote
------------------------


Delete your known_hosts file when an IP address changes
--------------------------------------------------------

Part of the security that the ssh suite of tools provides is monitoring the relationship between domain names, IP addresses, and the ssh keys. This is done to prevent a "main-in-the-middle-attack" where say a local network uses custom dhcp to rename a domain name (say your web server) to point to a different (and potentially evil) server that would then greedily lead you to login and eat your passwords for breakfast.

So, sometimes when you attempt to ssh, scp, or sshfs, you can get a scary message :

    murtaugh@battlestar:~$ ssh root@192.168.1.1
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
    Someone could be eavesdropping on you right now (man-in-the-middle attack)!
    It is also possible that a host key has just been changed.
    The fingerprint for the RSA key sent by the remote host is
    e5:da:3d:51:bb:70:6f:71:9c:b1:34:71:80:5c:27:b6.
    Please contact your system administrator.
    Add correct host key in /home/murtaugh/.ssh/known_hosts to get rid of this message.
    Offending RSA key in /home/murtaugh/.ssh/known_hosts:1
    RSA host key for 192.168.1.1 has changed and you have requested strict checking.
    Host key verification failed.

While it *might* be an attack, it might also simply mean that you've reassigned the IP address of a server, or changed the hardware or software setup. If such a change has occured, you simply need to make ssh forget the association. You could either remove the specific line in the file, or just blitz the whole file:

    rm ~/.ssh/known_hosts

And then, the first time you connect, you see that this gets recorded:

    murtaugh@battlestar:~$ ssh root@192.168.1.1
    The authenticity of host '192.168.1.1 (192.168.1.1)' can't be established.
    RSA key fingerprint is e5:da:3d:51:bb:70:6f:71:9c:b1:34:71:80:5c:27:b6.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added '192.168.1.1' (RSA) to the list of known hosts.

Look at known_hosts now:

    cat ~/.ssh/known_hosts


FAQ
----

* What's the difference between scp and ftp?

* What's the difference between ssh and telnet?

