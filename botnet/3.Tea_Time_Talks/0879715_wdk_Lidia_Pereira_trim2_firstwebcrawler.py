import urllib2, urlparse, urllib
import html5lib

yup = "http://www.online-literature.com"
urls = ["http://www.online-literature.com/author_index.php"]
history = []
rb = "rabo.txt"
rabo = open(rb,"a")
x = 0

def linkme(groupname,classname):
    for group in groupname:
        try:
            if group.attrib.get("class") == classname:
                searchable = group
                for mu in group:
                    links = mu.findall(".//a")
                    for link in links:
                        if link.attrib.get("href"):
                            href = link.attrib.get("href")
                            if href not in history and href.startswith(yup):
                                urls.append(href)
                                history.append(href)
        except httplib.IncompleteRead:
            print "Pfff!"

while urls:
    url = urls[x]
    urls.pop()
    f = urllib2.urlopen(url).read()
    parse = html5lib.parse(f, namespaceHTMLElements=False)
    group1 = parse.findall (".//tr")
    group2 = parse.findall (".//ul")
    mimimi = parse.findall(".//div")
    if x == 0:
        linkme(group1,"even")
    else:
        linkme(group2,"side-links")
    if mimimi:
        for mimi in mimimi:
            if mimi.attrib.get("id") == "chaptext":
                rabo.write(url.encode("utf-8")+"\n")

    x = x + 1