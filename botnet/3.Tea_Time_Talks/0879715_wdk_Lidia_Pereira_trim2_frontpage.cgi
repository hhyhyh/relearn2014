#!/usr/bin/env python
#-*- coding:utf-8 -*-
import cgi, urllib
import cgitb; cgitb.enable()
import urllib2, urlparse
import html5lib
import random, uuid, os

print "Content-Type: text/html"
print 
print """<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <link rel='stylesheet' type='text/css' href='/teatimetalks.css'>
    <link href='http://fonts.googleapis.com/css?family=Crimson+Text' rel='stylesheet' type='text/css'>
    <title>Tea Time Talks</title>
</head>
<img src ='/teatimetalk.gif' class ='header'/>
<body onload='document.getElementById('q').focus()''>
<form action='/cgi-bin/firstwebcrawler.cgi' class ='page' >
<input type='text' name='q' class ='april' placeholder='Search' size ='44'/> <br>
<input type='submit' value='' class ='search'/>
</form>
</body>
</html>"""

#generates a new session for each time you access the page
if not os.path.exists("TeaTimeTalk"):
    os.mkdir("TeaTimeTalk")
sessions = open("TeaTimeTalk/sessions.txt", "r+")
session = str(uuid.uuid1())
sessions.write(session) 

#shuffles the index so the results to avoid extreme repetition
index = open("rabo.txt")
shuffled= index.readlines()
random.shuffle(shuffled)
open("rabo.txt","w").writelines(shuffled)