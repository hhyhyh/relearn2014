from parsimonious.grammar import Grammar

grammar = Grammar(
    """
    command = "[" some lines "]"
    line = asoeuoae "->" asouhaoeu    


    bold_text  = bold_open text bold_close
    text       = ~"[A-Z 0-9]*"i
    bold_open  = "(("
    bold_close = "))"
    """)

grammar.


def parse(command):
    '''
    The command is a function of an angle that returns
    a function from angle to a series of line segments.

        str -> [(PYTHON,PYTHON)]

        str -> (ANGLE, LINE_NUMBER -> [(START,END)])

    '''
