import argparse
from threading import Thread

import chiplotle

from plotbot import PlotBot
from reporter import ReportPlotBot

parser = argparse.ArgumentParser(description='BashBot')
parser.add_argument('--host', default="localhost", help='host')
parser.add_argument('--port', type=int, default=6667, help='port')
parser.add_argument('observechannel', help='channel to join', default = '#2084')
parser.add_argument('plotchannel', help='channel to join', default = '#frontplotbot')
parser.add_argument('plotnickname', help='bot nickname', default = 'RonaldWeexy')
parser.add_argument('reportnickname', help='bot nickname', default = 'Circle')

def main():
    args = parser.parse_args()
    plotter = chiplotle.instantiate_plotters()[0]
    reporter = ReportPlotBot(args.observechannel, args.plotchannel, args.reportnickname, args.host, args.port, plotter = plotter)
    reporter.start()
#   plot = PlotBot(False, plotter, args.plotchannel, args.plotnickname, args.host, args.port)
#   Thread(None, reporter.start)
#   Thread(None, plot.start)
#   import time
#   while True:
#       time.sleep(1)
   

if __name__ == "__main__":
    main()
