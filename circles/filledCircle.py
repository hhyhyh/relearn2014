import math

from chiplotle.geometry.transforms.rotate import rotate
from chiplotle.geometry.transforms.offset import offset
from chiplotle.geometry.core.path import Path
from chiplotle.geometry.core.coordinate import Coordinate
from chiplotle.geometry.core.coordinatearray import CoordinateArray
from chiplotle.tools import plottertools
from chiplotle.hpgl import commands
from chiplotle import io
from chiplotle import instantiate_plotters

#plotter = plottertools.instantiate_virtual_plotter(type="HP7550A")

plotter = instantiate_plotters()[0]

margins = plotter.margins.soft
width = margins.right - margins.left
height = margins.top - margins.bottom

length = 100 * 40
steps = 150

plotter.write(commands.SP(1))
plotter.write(commands.VS(15))

for i in range(0, steps, 2):
    a = (float(i) / float(steps)) * math.pi
    line = Path([Coordinate(-length, 0) * .5, Coordinate(length, 0) * .5])
    rotate(line, a)
    offset(line, Coordinate(width * .5, height * .5))
    plotter.write(line)
    

plotter.write(commands.SP(0))

#io.view (plotter)