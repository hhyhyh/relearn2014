Bits and sound - 06.07-11.07.2014
======

### File formats
We can think of sound as series of air pressures; a wave (WAV) file
is sort of like a series of numerirc air pressure values.

We can think of image as a grid of lights; a netbpm (PPM, for example)
file is pretty much just a cartesian grid of colored pixels, where
each pixel's color is specified as a triplet of red, green, and blue values.

In an eight-bit wave file, each byte is the air pressure value for
a frame of audio. In a binary PPM file, each byte is the intensity
value for a particular color (red, green or blue) within a particular
pixel of the image.

### Color versus oscillation
I ran [this](http://small.dada.pink/netwaves/drums.py) to
produce a [file](http://small.dada.pink/netwaves/drums.ppm)
that looks like this when interpreted as ppm

XXX

and sounds like this when interpreted as an 8-bit wave file
played at 8000 hz.

XXX

Sound is expressed as oscillations in value, whereas perceived color
is expressed the average value in a particular region of the image.
A series of pixels of the same value is heard as silence, and the
same series of pixels is viewed as a solid color. The file above sounds
like a series of beeps, and the changes in visual color aren't really
noticable when the file is played as audio.

#http://source.thomaslevine.branchable.com/?p=source.git;a=blob_plain;f=dada/relearn/index.mdwn;hb=HEAD


Thomas Levine

XXXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXX
XXXXXXX
XXXX


## documentation

On  Tuesday evening, Yves Poliart was invited to present a playlist in echo to the worksession.
He spoke from a text file containing small text entries shortly describing the musicians accompanied by urls, leading to youtube more often than not. 
It was quite exciting to quickly go through works by Luigi Russolo, Walter Rutmann, Daphné Oram and Delia Derbyshire, Coil, and quite a few others. 
##documentation

## loops
You'll find here a set of python script to write ppm patterns pixels by pixels.
exemple:
`
# Generates a gradient image in the ppm format
# Usage:
#     python gradient.py > myimage.ppm
print('P3')
print('51 5')
print('255')
for value in range(255):
    print('0 0 ' + str(value))
`
looks like
<img src="/loops/gradiant.ppm" alt="a blue gradiant"/>
## sheetmusic
Sheetmusic is a plugin of Gnumeric providing musical functions inside the spreadsheet.
https://pypi.python.org/pypi/sheetmusic?
— The spreadsheet 'rgbvaluestochords.gnumeric' uses the following formula to map rgb values (decimals from 0 to 255) on a base 7 corresponding to 7 notes (from A to G) and 7 octaves.
=concatenate(if(roundup(mod(A2/256*49,7))<2,"A",if(roundup(mod(A2/256*49,7))<3,"B",if(roundup(mod(A2/256*49,7))<4,"C",if(roundup(mod(A2/256*49,7))<5,"D",if(roundup(mod(A2/256*49,7))<6,"E",if(roundup(mod(A2/256*49,7))<7,"F","G")))))),if(roundup(A2/256*49/7)=0,1,roundup(A2/256*49/7)))
This means with this spreadsheet you can convert a pixel into a chord.
— 'rgbvaluestochords.gnumeric' convert decimals to notes. Play Fibonacci !
## pd

Pure data patches.
binaryseqauencer: play sound from picture with a binary sequencer 
play sound from a text and/or with the keyboard
pixel-player: play sound from rgb pixel datas
asciify: turns characters (symbols) into ascii values (floats)
list-find: Find positions of a value (symbol or float) in a list
rawreader
open textfile
textfileludipaul


=====================================================================================================
Digital Drawdio --> connecting pencil drawings to all kinds of programmes and networks by dy-wen
==============================================================================
Arduino takes the resistive qualities of the black crayons and it can send it as serial data to 
- pure data
- the terminal
- python
- the network
- chatbots?
- somewhere in space??
Setting up the Arduino in physical space
==================================
- take a 800KOhm resistor and make a voltage divider with the A0 as Arduino input (change the resistor value if necessary)
- put a pin in the graphite of the pencil & connect it to the Arduino with a aligator clip (this is one leg of your variable resistor in your voltage divider)
- connect a aligator clip to the drawing (onto the graphite) -> it is your ground 
--> start drawing between the ground and further on the paper
Arduino & Pure Data-extended
==========================
- on Arduino, upload the StandardFirmata_slower sketch
- to connect with the Arduino in Pure Data --> put the Pduino_arduino_test.pd patch in the folder where your music patch is
- open it and follow these instructions
- Instructions to connect the Arduino data to your Pure Data Patch with Pduino 
------------------------------------------------------------------------------------------------------------
- Choose the right serial port, for the usb to connect to the Arduino (little blocks in the top left of this patch) 
- Open the connections to the Arduino (analog --> 14 & 15) in the top right box (configure mode for each pin) 
- you should see values changing at the bottom left - there are two send objects: s arduino_analo0 & s arduino_analog1 
- put two receive patches in your own pd patch to receive these data (r arduino_analo0 & r arduino_analog1) 
arduino & the terminal::
============================
Put a sketch on your Arduino that sends serial data to the serial monitor
In a terminal -> run:
stty -F /dev/ttyACM0 9600
stty -F /dev/ttyACM0 raw
cat < /dev/ttyACM0
(potentially interesting: https://github.com/todbot/arduino-serial/)
Arduino & Python
========================
http://playground.arduino.cc/interfacing/python
Arduino and Python
Talking to Arduino over a serial interface is pretty trivial in Python. On Unix-like systems you can read and write to the serial device as if it were a file, but there is also a wrapper library called pySerial that works well across all operating systems.
After installing pySerial, reading data from Arduino is straightforward:
>>> import serial
>>> ser = serial.Serial('/dev/ttyNameOfYourSerialForArduino', 9600)
>>> while True:
...     print ser.readline()

=============================================================================
Commenting on bits-and-sound :: a partial account of the week through day 1 
:: beware, *syntactic sugar* only :: by madeleine
=============================================================================

==the endless translation workshop==

During this week we have been switching formats and interfaces treating data versatility to its limits / bringing out its potential. We have been producing data then working it into different formats and so into different lectures and devices. (Earphoned) ears, (concentrated) eyes, (busy) hands and (mostly sitting) bodies have been playfully manipulating the protean lives of 0s and 1s in the name of sound. 
Here, what-you-see-is-what-you-hear-is-what-is-transformable-is-what-keeps-rewriting-itself.

During this first day some people within the group were experimenting with glitching for the first time, some others were using the terminal instead of a custom software to produce them for the first time while others were already beyond simple glitching and into DIY experiments for instance (hi Wendy!). 
Some collections of glitched images ended up as animated gifs produced in the commandline : the "broken down" images came alive like golums of code through a simple suite of words : convert abcd.jpg abcd.gif

## monday-morning-glitches==>>
This folder has gathered our first experiments editing pictures while playing with software, be it Vim or convert, nano and gimp in the terminal, etc.
This session helped us come a bit closer to the basic structure of several picture formats. We started by identifying headers, content and other parts of the data contained within images by opening them into "foreign" software. 
Opening data (i.e. an image file) "where it does not belong" (i.e. a text editor) 
(((but, from the PoV of data, 
are there really such places given its inherent versatility?))) 
is like forcing a "breakdown" = "force quit" but stay put. 

Through this artificially provoked malfunction what actually happens is that some things do break down. And they break down in many ways : 

On the one hand, 
the image file is no longer a meaningful, more or less homogeneous surface you can have a quick look at, it not longer "works" for your (codewise illiterate) senses, it has become a broken (down) image. 

On the other hand, 
this image is literally broken down as it is now in pieces, reduced to its bits and pieces. It has become (un)readable textual material and, though visually rich, unable to be perceived as a traditional image, at least not by human eyes. 
This discontinuous writing is the image as it is (now) : broken down to the pieces that tell the computer how to show it to human users. To human eyes many characters are familiar while some look definitely strange and some are more like new combinations of known elements. This state of the file is an open door that leads to a further (third) breaking down also known as glitching.

Why did glitching come up so quickly in the bits-and-sound room? And, how come there is so much glitszching going on nowadays on the web for instance? What do you do when you glitch? To glitzch is to intentionnaly provoke a breakdown, isn't it. You start experimenting with formats and files, you start gliTching. When you turn for instance an image into text by choosing to open it in a text editor rather than in a dedicated image editor, what you make happen is not only transforming the perception of the file, but also stripping the body of a contemporary digital file "naked". 
Images in text editors become as many half-readable half-unreadable chunks of text (letters, symbols, numbers) as you can mistreat, bend, erase, tweak, transfer -up to the point when (if you distort the header for instance) the file really breaks down and can be opened nowhere no more. 
But before it becomes a lonely dead piece of text, it will have become a variation of itself. 
And also a variation of possible relations to the computer (that can thus become a start for the way to *companionship* ((cf. D. Haraway)), to the way it computes and how it uses us/we use it or make things with each other. By treating files "deviantly" (yet harmlessly?), we get to strip the user interface down to a first layer of code. We get to know tech-logic a bit better, we meet the noise. Provoking a system's breakdown makes the system visible ((the idea is several decades old, comes from ambiguous Heidegger)). 
Picture for instance electricity going down while you're in a museum or some other windowless place. You suddenly remember it's so useful and fragile. You realise how electric current makes som any modern things work : the museum as a space, your activities there, the way you see art, the way you talk about it online. Rathen than banal and everywhere (it's not everywhere, except in its bio-electrical form), electricity appears for what it is = a reality-forging technology (to go very very fast). So, forcing a file into a "foreign" environment (=the wrong software) exposes the fact that what we recognise as sound, image and text through our different (often codewise illiterate) senses are code and layers of translations that go as far down as on/off == true/false == 0/1.
In an apple/google/samsung/facebook/etc. world, this realisation can amount to important and even dangerous data. Not because the above-mentioned superpowers are afraid of glitched files and animated gifs of course, but because code literacy (which can start with glitching and experimenting bits and sounds in unsound ways), combined with critical thinking, can become a way to destabilise apple/google/samsung/facebook/etc.'s current and coming kingdoms. But glitzching in this political sense is also important on a more humble mundane level opening up possible companionships that do much more than simply allow apple/google/samsung/facebook/etc. to augment their gains and power, which seems to be the dominant reason software is created these days... let's glitzsch the landlord, let's lyntzch the landlord yeah 


P.S. twitter says On Kawara died today, 11.07.2014, age 81.

=======   ===============    ========================================  ==== ====
===  =========================================     ==============  ======= =====
