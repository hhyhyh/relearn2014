from __future__ import print_function
import json, re, os
from urllib2 import urlopen
from urllib import urlencode
from pprint import pprint
from urlparse import urljoin
import sys, argparse
import html5lib
from xml.etree import ElementTree as ET
import urllib2

"""

Save all pads (text) to .text files.

Usage:

python paddump.py --api-key relearn.local.apikey http://relearn

"""

urlopener = urllib2.build_opener()
urlopener.addheaders = [('User-agent', ' Mozilla/5.0 (X11; Linux i686; rv:30.0) Gecko/20100101 Firefox/30.0')]


def listAllPads (apiURL, apikey):
    data = {'apikey': apikey}
    resp = json.load(urlopen(apiURL+'listAllPads?'+urlencode(data)))
    return resp['data']['padIDs']

def getPadText (padID, apiURL, apikey):
    data = {'apikey': apikey, 'padID': padID}
    resp = json.load(urlopen(apiURL+'getText?'+urlencode(data)))
    return resp['data']['text']

def dump (apiurl, apikey, start_pad_id=None, css_pad_id=None, output="./pages"):

    # load all pad ids for use in marking red links (and knowing what links not to follow)
    all_padIDs = listAllPads(apiurl, apikey)
    all_padIDs.sort()
    for padID in all_padIDs:
        print (u"DUMPING: {0}".format(padID).encode("utf-8"), file=sys.stderr)
        src = getPadText(padID, apiurl, apikey)
        with open(os.path.join(output, padID+".text"), "wb") as fout:
            print (src.encode("utf-8"), file=fout)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('padURL', help='the URL of the etherpad server')
    parser.add_argument('--padID', help='the name of the pad you want to dump (default is all)')
    parser.add_argument('--html', action="store_true", help='export HTML')
    parser.add_argument('--pad-port', default="9001", help='port of pad server')
    parser.add_argument('--api-key', default="APIKEY.txt", help='file with API key to use')
    parser.add_argument('--api-version', default="1.2.9", help='the version of the etherpad api')
    parser.add_argument('--api-url', default="/api/", help='URL path to the API')
    parser.add_argument('--css', default="Common.css", help='pad ID for stylesheet (default Common.css)')
    parser.add_argument('--output', default="./pages", help='path for output pages (default ./pages)')
    args = parser.parse_args()

    apikey = open(args.api_key).read()
    padurl = args.padURL
    if not padurl.startswith("http"):
        padurl = "http://" + padurl
    if args.pad_port:
        padurl += ":{0}".format(args.pad_port)
    if args.api_url.startswith("http"):
        apiurl = args.api_url
    else:
        apiurl = "{0}{1}{2}/".format(padurl, args.api_url, args.api_version)

    # start_pad_id = name_to_pad_id(args.padID)
    dump(apiurl, apikey, start_pad_id=args.padID, css_pad_id=args.css, output=args.output)
