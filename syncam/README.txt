*
*SYNCAM :: Relearn 2014
This worksession proposes to experiment − and document − the construction of DIY tools to record and display audio-visuals in order to open, as much as possible, a comprehension of the multimedia workflow in a creative way.  
Considering that a tool has an influence on its usage and vice-versa,   what happens if you stick to this cause-and-effects process by building  our tool ?
Device can be build around a raspberry pi (micro computer), that will be  augmented by autonomous modules that act as synaesthetic functions  communicating with each other in real time. As the display also  influence the way of filming, a whole dispositif to record and display  should be considered. The outcomes being forseen as experimental, the worksession will also be  about feeding the imagination about what a camera could be, while  documenting the discoveries and/or hypothesis. 

http://f-lat.org/kiwi/doku.php/fr:edu:relearn:syncam::start

*Useful commands

you can list available usb devices with 
*lsusb

record audio input
*arecord -f cd audio.wav 

test your speakers
*speaker-test

set volume
*alsamixer

playback audio file
*aplay -f cd file.wav


*Gstreamer installation on the π

*installation
*to install gstreamer-0.10
*sudo apt-get install gstreamer-tools gstreamer0.10-plugins-bad gstreamer0.10-plugins-ugly gstreamer0.10-plugins-good gstreamer-ffmpeg gstreamer0.10-alsa

*to install gstreamer 1.0
edit the file  /etc/apt/sources.list 
	sudo nano /etc/apt/sources.list 
and add the following in a new line
	deb http://vontaene.de/raspbian-updates/ . main

then do

	sudo apt-get update
*
	sudo apt-get install libgstreamer1.0-0 libgstreamer1.0-0-dbg libgstreamer1.0-dev liborc-0.4-0 liborc-0.4-0-dbg liborc-0.4-dev liborc-0.4-doc gir1.2-gst-plugins-base-1.0 gir1.2-gstreamer-1.0 gstreamer1.0-alsa gstreamer1.0-doc gstreamer1.0-omx gstreamer1.0-plugins-bad gstreamer1.0-plugins-bad-dbg gstreamer1.0-plugins-bad-doc gstreamer1.0-plugins-base gstreamer1.0-plugins-base-apps gstreamer1.0-plugins-base-dbg gstreamer1.0-plugins-base-doc gstreamer1.0-plugins-good gstreamer1.0-plugins-good-dbg gstreamer1.0-plugins-good-doc gstreamer1.0-plugins-ugly gstreamer1.0-plugins-ugly-dbg gstreamer1.0-plugins-ugly-doc gstreamer1.0-pulseaudio gstreamer1.0-tools gstreamer1.0-x libgstreamer-plugins-bad1.0-0 libgstreamer-plugins-bad1.0-dev libgstreamer-plugins-base1.0-0 libgstreamer-plugins-base1.0-dev
	
*look at your webcam using gstreamer
*gst-launch-0.10 v4l2src device=/dev/video0 ! ffmpegcolorspace ! xvimagesink
*
*grab a picture from your webcam using gstreamer
*gst-launch-0.10 v4l2src ! ffmpegcolorspace ! pngenc ! filesink location=image.png


*FFmpeg
to install simply do
*sudo apt-get install ffmpeg 

*simple audio capture with ffmpeg
*ffmpeg -f alsa -i default audio.wav

*simple video capture with ffmpeg
*ffmpeg -f video4linux2 -s 320x240 -i /dev/video0 video.ogv


Video4linux2Loopback

sudo apt-get install v4l2loopback-dkms
modprobe v4l2loopback

*
*STREAM

*stream audio with gstreamer on an icecast server
gst-launch-0.10 pulsesrc device=alsa_output.pci-0000_00_1b.0.analog-stereo.monitor  !  audioconvert  ! opusenc ! oggmux ! shout2send ip=192.168.100.114 port=8000  password=syncam mount=syncam01.ogg

stream video vers Icecast with the Rapicam
(marche pas )

raspivid -t 0 -fps 15 -w 640 -b 750000 -h 480 -o - |  gst-launch-1.0 -v fdsrc !  h264parse  !  queue  !  videoconvert ! theoraenc quality=16 ! queue ! oggmux name=mux  alsasrc ! audio/x-raw-int,rate=8000,channels=1,depth=8 ! queue  !  audioconvert  !  vorbisenc  !  queue  !  mux. mux. !   queue ! shout2send ip=192.168.100.114 port=8000 password=syncam mount=/whatever.ogg


Projects
Stéphane (Noël)
1) capture an image from the webcam, store it with a unique name (based on date ?)

for 1 jpg image

ffmpeg -f video4linux2 -i /dev/video0 -vframes 1 capture_$(date +'%d%m%y').jpg

2) launch capture and download from an another computer in ssh

3) transform an image in ascii and send it through irc
4) from an irc message, launch capture + ascii transform + send line by line through irc

*Capture an image and transfer it from pi to mac
*What i have done
The idea is to launch a single command from the mac to the raspberry pi that capture an image from the webcam, and transfer this image to the mac
Hardware : mac book pro - connected through ethernet to - raspberry pi with usb camera 

On Raspberry pi
installed raspbian
hostname achille (username still pi)
enabled ssh
install : ffmpeg, guvcview, libnss-mdns (en utilisant la commande sudo apt-get install <nom du programme>

connected the raspeberri with ethernet, share internet in the system prefs from airport to all via ethernet

wrote a file commande.sh in /home/pi

// code de commande.sh
#!/bin/bash
echo "je capture une image depuis la webcam"
nom_fichier=capture_$(date +'%d%m%y%H%M%S').jpg
echo "Son nom est $nom_fichier"

ffmpeg -f video4linux2 -i /dev/video0 -vframes 1 $nom_fichier
echo "je tente d'envoyer l'image sur l'ordinateur de stéphane dans le dossier 'documents'"
scp $nom_fichier stephane@MacBook-Pro-de-stephane-noel.local:Documents
// end code
! attention ! stephane@MacBook-Pro-de-stephane-noel is my machine. ":Documents" is the path to the place where you want to store the image
! attention ! no space between the user and the path as you can see in the code above
about scp : http://www.hypexr.org/linux_scp_help.php

make this script executable by Pi
// terminal code
chmod +x commande.sh
// end terminal code

at this point, the code can be execute on the pi
// terminal code
./commande.sh
// end code

On the Mac
activate in system prefs "distant session" (activer session à distance) for all user
open terminal and call pi by ssh 

// terminal code
ssh pi@achille.local
//end code
enter the pi password
call the script
// terminal code
./commande.sh
// end code

(passwords required in both way)

Alternative to this solution on pi website : http://www.raspberrypi.org/learning/webcam-timelapse-setup/

Convert an image from the webcam into ascii message
add jp2a to rapsberry
the library need a jpeg file in input, and output a text file
// code
jp2a --width=60 file.jpg --output textfile.txt
// end code

Uploader l'image vers un serveur web par ftp
Ajouter ftp avec apt-get ftp


Pierre
1. Access remotely to raspicam
connect through ssh pi@192.168.100.XXX
sudo apt-get install libnss-mdns
exit
ssh pi@newname.local
install ffmpeg and gstreamer (see above)
2. First capture! yes!
raspistill -t 2000 -o image.jpg 
copy it to local computer over network
scp image.jpg pierreh@192.168.100.105:~/Bureau/relearn/image.jpg
to use a graphical interface on the raspberry
exit from ssh and ssh -X pi@raspraster.local
midori
then open the file with url bar
3. Run camera forever, taking a picture when Enter is pressed
*raspistill -t 0 -k -w 640 -h 480 -o pics%02d.jpg
4. 20 pixels height seems the minimum for raspivid, so imagemagick
convert pics01.jpg -resize 640x1\! pics011.jpg
5. chain
*raspistill -t 0 -k -w 640 -h 480 -o pic.png –e png; convert pic.png -resize 640x1\! pic1.png

en cherchant les trames gif d'imagemagick
http://www.imagemagick.org/Usage/quantize/#diy_symbols
tramage avec des symboles



python bot to capture

----

import socket
import os

class IRCClient:
    socket = None
    connected = False
    nickname = 'OlBot'
    channels = ['#2084', '#syncam']
    asciiart = []


    def __init__(self):
        self.socket = socket.socket()
        self.socket.connect(('192.168.1.222', 6667))
        self.send("NICK %s" % self.nickname)
        self.send("USER %(nick)s %(nick)s %(nick)s :%(nick)s" % {'nick':self.nickname})

        while True:
            buf = self.socket.recv(4096)
            lines = buf.split("\n")
            for data in lines:
                data = str(data).strip()

                if data == '':
                    continue
                print "I<", data
                if data.find('capture') != -1:
                   asciiart = os.system('raspistill -t 1 -w 320 -h 240 -o - | jp2a --height=16 - | tee ascii.txt')
                   file = open('ascii.txt', 'r')
                   for i in range(1,16):
                       self.say(file.readline(), '#2084')
                       sleep(0.01)
                # server ping/pong?
                if data.find('PING') != -1:
                    n = data.split(':')[1]
                    self.send('PONG :' + n)
                    if self.connected == False:
                        self.perform()
                        self.connected = True

                args = data.split(None, 3)
                if len(args) != 4:
                    continue
                ctx = {}
                ctx['sender'] = args[0][1:]
                ctx['type']   = args[1]
                ctx['target'] = args[2]
                ctx['msg']    = args[3][1:]

                # whom to reply?
                target = ctx['target']
                if ctx['target'] == self.nickname:
                    target = ctx['sender'].split("!")[0]

                # some basic commands
                if ctx['msg'] == '!help':
                    self.say('available commands: !help', target)

                # directed to the bot?
                if ctx['type'] == 'PRIVMSG' and (ctx['msg'].lower()[0:len(self.nickname)] == self.nickname.lower() or ctx['target'] == self.nickname):
                    # something is speaking to the bot
                    query = ctx['msg']
                    if ctx['target'] != self.nickname:
                        query = query[len(self.nickname):]
                        query = query.lstrip(':,;. ')
                    # do something intelligent here, like query a chatterbot
                    #print 'someone spoke to us: ', query
                    #self.say('alright :|', target)

    def send(self, msg):
        print "I>",msg
        self.socket.send(msg+"\r\n")

    def say(self, msg, to):
        self.send("PRIVMSG %s :%s" % (to, msg))

    def perform(self):
        #self.send("PRIVMSG R : Register <>")
        self.send("PRIVMSG R : Login <>")
        self.send("MODE %s +x" % self.nickname)
        for c in self.channels:
            self.send("JOIN %s" % c)
            # say hello to every channel
            self.say('hello world!', c)

IRCClient()

----


*Streaming Video

*Simple video streaming from Raspberry Pi to your computer with the raspicamera module and netcat
(you may want to enable the camera module in the raspi configuration)
*sudo apt-get install netcat
 PI camera data is send with udp using nc (netcat) :
    - pi side:
*        raspivid -t 0 -vf -w 480 -h 360 -fps 25 -b 800000 -n -o -  | nc -u YOUR_IP_ADDRESS YOUR_PORT
    - computer side:
*       nc -lu  YOUR_PORT | mplayer -fps 31 -cache 1024 -

*Stream video to IceCast server
First install oggfwd and ffmpeg2theora
*sudo apt-get install oggfwd ffmpeg2theora

Send your webcam to Icecast server:
*with an usb webcam :
*ffmpeg2theora /dev/video0 -f video4linux2 --noaudio -o - | oggfwd 192.168.1.222 8000 relearn /YOURMOUNTPOINT.ogv
*with the raspicam
*raspivid -t 0 -vf -n -w 480 -h 260 -fps 25 -b 800000 -o - | ffmpeg2theora --noaudio -o /dev/stdout - | oggfwd 79.99.202.57 8000 password /pi-cam.ogv

*with an usb webcam and Gstreamer
*gst-launch-0.10 v4l2src ! 'video/x-raw-yuv,width=320,height=240,framerate=15/1' ! videorate max-rate=1 ! queue ! theoraenc !  oggmux ! shout2send ip=79.99.202.57 password=xxxxxx mount=variable.ogg


TODO
(stream in progress)
TX
raspivid -t 0 -w 480 -h 260 -fps 25 -b  500000 -vf -n -o - | ffmpeg -i - -f alsa -i hw:1 -vcodec copy -acodec aac -strict experimental -f h264 udp://192.168.100.102:1234

RX (how to reveive it)
ffplay udp://@:1234


<<<< TODO
Make a conversion locally on the server

on the PI: raspivid -t 0 -vf -w 480 -h 260 -fps 25 -b 800000 -n -o -  | nc SERVER_IP SERVER_PORT
on the Server: nc -l SERVER_PORT | ffmpeg2theora --noaudio -o /dev/stdout - | oggfwd 127.0.0.1 8000 ICECAST_PASSWORD /MOUNTPOINT.ogv


